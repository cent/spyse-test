# coding: utf-8
from __future__ import division
import argparse
import random
import socket
import string
import struct
import sys
import uuid
from datetime import datetime
from ipaddress import IPv6Network, IPv6Address
from time import sleep

from jinja2 import FileSystemLoader, Environment

import tasks


__author__ = 'vadim'


class RandomData:
    """
    RandomData help to generate random data
    """
    @property
    def random_domain(self):
        name = ''.join(random.choices(string.ascii_lowercase, k=5))
        tld = ''.join(random.choices(string.ascii_lowercase, k=3))
        return '.'.join([name, tld])

    @property
    def current_timestamp(self):
        return datetime.now().isoformat()  # '%Y-%m-%d %H:%M:%S.%f'

    @property
    def random_ipv4_address(self):
        return socket.inet_ntoa(struct.pack('>I', random.randint(1, 0xffffffff)))

    @property
    def random_ipv6_address(self):
        network = IPv6Network('2001:cafe::/64')
        return IPv6Address(network.network_address + random.getrandbits(network.max_prefixlen - network.prefixlen))

    @property
    def random_uuid4(self):
        return str(uuid.uuid4())


def gen(count, packet_size, freq, template_data):
    """
    gen - function generate and send random data
    :param count: count data generate
    :param packet_size: packet size of data
    :param freq: count data in second
    :param template_data: templates
    """
    send_time = datetime.now()
    dt = 1 / freq

    def wait_send_data(d):
        mqs = dt - (datetime.now() - send_time).microseconds
        if mqs > 0:
            sleep(mqs / 1000000)
        send_data(d)

    rnd = RandomData()

    packet_data = []
    for k in range(count):
        t = random.choice(template_data)
        data = t.render(
            RANDOM_DOMAIN=rnd.random_domain,
            CURRENT_TIMESTAMP=rnd.current_timestamp,
            RANDOM_IPV4_ADDRESS=rnd.random_ipv4_address,
            RANDOM_IPV6_ADDRESS=rnd.random_ipv6_address,
            RANDOM_UUID4=rnd.random_uuid4,
        )
        packet_data.append(data)
        if (k+1) / packet_size == 0 and packet_data:
            wait_send_data(packet_data)  # Send
            send_time = datetime.now()
            packet_data = []

    if packet_data:
        wait_send_data(packet_data)  # Send


def send_data(data):
    """
    send_data - function send data to ETL
    """
    # tasks.process_data.delay(data)
    tasks.process_data(data)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--count', type=int, nargs='?', default=100, help='общее количество записей')
    parser.add_argument('--packet-size', type=int, nargs='?', default=10, help='количество записей в пакете')
    parser.add_argument('--freq', type=int, nargs='?', default=1, help='количество пакетов в минуту')
    flags = parser.parse_args(sys.argv[1:])

    env = Environment(loader=FileSystemLoader('template'))
    template_data = [env.get_template(n) for n in ('normal_data.jinja', 'empty_data.jinja')]

    gen(flags.count, flags.packet_size, flags.freq, template_data)
    print("Send data.")
