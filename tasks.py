# coding: utf-8
import json
import os
from collections import defaultdict

from celery import Celery
from celery.utils.log import get_logger
from elasticsearch import Elasticsearch

__author__ = 'vadim'

logger = get_logger('celery')

app = Celery('tasks', broker=os.environ.get('ST_RABBIT', 'amqp://guest:guest@localhost:5672//'))

es = Elasticsearch(os.environ.get('ST_ELASTIC'))


@app.task
def process_data(packet_data):
    """
    process_data make transform data
    :param packet_data: data from generator
    """
    bulk_size = 100
    result = []
    bulk_data = []
    count = 0
    for p in packet_data:
        packet = json.loads(p)

        if packet['status'] != 'NOERROR':
            continue

        try:
            data = {
                'name': packet['name'],
                'dns_records': defaultdict(list),
                'dns_records_updated_at': packet['timestamp'],
            }

            for answer in packet['data']['answers']:
                if answer['type'] not in ('A', 'AAAA', 'MX', 'TXT'):
                    continue
                data['dns_records'][answer['type']].append(answer['answer'])

            bulk_data.append(data)
            count += 1
            if count >= bulk_size:
                result.append(bulk_data)
                bulk_data = []

        except Exception as e:
            logger.exception('Can not parse structure: %s', packet)

    if len(bulk_data) > 0:
        result.append(bulk_data)

    if result:
        for bulk_data in result:
            save_data.delay(bulk_data)


@app.task
def save_data(data):
    """
    save_data function save data to database
    :param data: formatted data
    """
    if 'ST_DISABLE_SAVE' in os.environ:
        logger.info("Disable save data")
        return

    index = 'dns_records'
    if not es.indices.exists(index=index):
        body = json.load(open(os.path.join(os.path.dirname(__file__), 'dns_records.json'), 'r'))
        es.indices.create(index=index, body=body)
        pass

    for d in data:
        es.index(index=index, id=d['name'], body=d)
