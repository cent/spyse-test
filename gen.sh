#!/bin/sh

docker run -it --network spyse-test_backbone \
  -e ST_RABBIT=amqp://guest:guest@rabbit:5672// \
  -e ST_ELASTIC=http://elasticsearch:9200 \
  --rm spyse_test python gen.py $@

